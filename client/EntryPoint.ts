
// Copyright (c) 2018 DBotThePony

// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons
// to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be
// included in all copiesor substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

if (process.argv.length == 2) {
	console.log(`You must specify HTTP(s) URL link as parameter of node next to javascript filename with package's json index (bundle_index.json) on the server.`)
	process.exit(0)
}

let connections = 32
let workingDir = './'
import fs = require('fs')

for (let i = 3; i < process.argv.length; i++) {
	if (process.argv[i] == '--connections') {
		if (!process.argv[i + 1]) {
			throw new TypeError('Missing argument for --connections')
		}

		const num = Number(process.argv[i + 1])

		if (num != num || num < 1) {
			throw new TypeError('Malformed argument for --connections')
		}

		connections = num
		i++
		continue
	}

	if (process.argv[i] == '--output') {
		if (!process.argv[i + 1]) {
			throw new TypeError('Missing argument for --output')
		}

		try {
			fs.mkdirSync(process.argv[i + 1])
		} catch(err) {

		}

		workingDir = process.argv[i + 1]
		i++
		continue
	}
}

if (workingDir[workingDir.length - 1] != '/') {
	workingDir += '/'
}

const index = process.argv[2]
const split = index.split('/')
split.pop()
const relativeDir = split.join('/') + '/'
const bundle = relativeDir + 'bundle.json'

import {HTTPClient} from './HTTPClient'

const client = new HTTPClient(connections)

import {createRotation, createNewTimer} from '../shared/ConsoleStuff'

createNewTimer(createRotation('Fetching index'))

import {PackageBundle} from '../shared/PackageBundle'
import {PackageIndex} from '../shared/PackageIndex'
import {PackageFile} from '../shared/PackageFile'
import {SimWorkerDispatcher} from '../shared/SimWorkerDispatcher'

const bodies: string[] = []

client.download(index, (chunk: Buffer) => {
	bodies.push(chunk.toString('utf8'))
})
.then(() => {
	const packageBody = bodies.join('')
	const json = JSON.parse(packageBody)

	const index = new PackageIndex(json, true)
	const bundleBodies: string[] = []

	createNewTimer(createRotation('Fetching bundle'))

	client.download(bundle, (chunk: Buffer) => {
		bundleBodies.push(chunk.toString('utf8'))
	})
	.then(() => {
		const bundleBody = bundleBodies.join('')
		const bundle = new PackageBundle(true, workingDir)
		bundle.from(bundleBody)

		index.search(workingDir).then(currentFiles => {
			const toremove: string[] = []
			const todownload: PackageFile[] = []
			const toverify: PackageFile[] = []
			const pathindex: string[] = []
			const files = bundle.files()

			createNewTimer(createRotation('Detecting old files...'))

			for (const entry of files) {
				pathindex.push(entry.path)
			}

			for (const existing of currentFiles) {
				if (!pathindex.includes(existing) && index.shouldRemove(existing)) {
					toremove.push(existing)
				} else if (pathindex.includes(existing) && index.shouldVerify(existing)) {
					const arr = files[pathindex.indexOf(existing)]
					toverify.push(new PackageFile(bundle, existing, arr.sha256, true, arr.size))
				}
			}

			for (const needed of pathindex) {
				if (!currentFiles.includes(needed)) {
					const arr = files[pathindex.indexOf(needed)]
					todownload.push(new PackageFile(bundle, needed, arr.sha256, index.isSync(needed), arr.size))
				}
			}

			createNewTimer(createRotation('Deleting files...'))

			const callback = () => {
				const callback = () => {
					if (todownload.length == 0) {
						console.log('\nSuccessfull finish of verification. No files are needed to be downloaded')
						process.exit(0)
						return
					}

					let done = 0
					let doneStreams = 0
					let sizeDone = 0
					let sizeTotal = 0
					const total = todownload.length

					const streamWorker = new SimWorkerDispatcher<PackageFile>(256, todownload, (file) => {
						return new Promise((resolve, reject) => {
							file.createDirs().then(() => {
								const stream = file.openWriteStream()

								client.download(relativeDir + encodeURI(file.path).replace(/\#/g, '%23'), (chunk: Buffer) => {
									stream(chunk)
									sizeDone += chunk.length
								}).then(() => {
									file.closeStream().then(() => {
										resolve()
										doneStreams++
									})

									done++

									if (done == total) {
										createNewTimer(() => {
											return `Finishing writing all files... [${doneStreams}/${total} written to the disk; ${streamWorker.active}/${streamWorker.maxWorkers} file streams]`
										})
									}
								}).catch(err => {
									console.error('Failed to download file ' + file.path + '!')
									console.error(err)
									reject(err)
									process.exit(1)
								})
							}).catch(err => {
								console.error('Failed to create all directories needed for file ' + file.path + '!')
								console.error(err)
								process.exit(1)
							})
						})
					}, () => {
						console.log('\nAll files are downloaded and are written to the disk! Woo!')
						process.exit(0)
					})

					for (const file of todownload) {
						if (file.filesizeExpected) {
							sizeTotal += file.filesizeExpected
						}
					}

					streamWorker.work()

					sizeTotal = Math.floor(sizeTotal / 1024)

					createNewTimer(() => {
						return `Downloading files... [${Math.floor(sizeDone / 1024)} kb/${sizeTotal} kb; ${done}/${total} downloaded; ${doneStreams}/${total} finished; ${client.workingConnections}/${client.connections} DL channels]`
					})
				}

				createNewTimer(createRotation(`Verificating files... (by ${index.noStrictChecks && 'file sizes' || 'hash sums'})`))

				if (toverify.length == 0) {
					callback()
				} else {
					if (index.noStrictChecks) {
						new SimWorkerDispatcher<PackageFile>(16, toverify, (file) =>
							new Promise((resolve, reject) => {
								if (file.filesizeExpected == null) {
									console.error('At least one of files are lacking size in bundle.')
									process.exit(1)
								}

								file.resolveSize().then(() => {
									if (file.outOfSyncWeak()) {
										todownload.push(file)
									}

									resolve()
								}).catch(err => {
									console.log(err)
									reject(err)
								})
							})
						, () => {
							callback()
						}).work()
					} else {
						new SimWorkerDispatcher<PackageFile>(4, toverify, (file) => {
							return new Promise((resolve, reject) => {
								if (file.hashsumExpected == null || file.hashsumExpected == undefined) {
									console.error('At least one of files are lacking sha256 in bundle.')
									process.exit(1)
								}

								file.resolveSize().then(() => {
									if (file.outOfSync()) {
										todownload.push(file)
									}

									resolve()
								})
							})
						}, () => {
							callback()
						}).work()
					}
				}
			}

			if (toremove.length == 0) {
				callback()
			} else {
				new SimWorkerDispatcher<string>(256, toremove, (file) => {
					return new Promise((resolve, reject) => {
						fs.unlink(workingDir + file, (err) => {
							if (err) {
								console.error('Error when removing old files!')
								console.error(err)
								process.exit(1)
							}

							resolve()
						})
					})
				}, () => {
					callback()
				}).work()
			}
		})
	})
	.catch((err) => {
		console.error('Error when downloading file bundle!')
		console.error(err)
		process.exit(1)
	})

})
.catch((err) => {
	console.error('Error when downloading index file!')
	console.error(err)
	process.exit(1)
})
