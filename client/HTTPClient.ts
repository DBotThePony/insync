
// Copyright (c) 2018 DBotThePony

// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons
// to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be
// included in all copiesor substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

import http = require('http')
import https = require('https')
import urlModule = require('url')

interface QueueTable {
	onchunk: (chunk: Buffer) => void
	onStart?: (response: http.IncomingMessage) => void
	url: urlModule.URL
	https: boolean
	agent: http.Agent | https.Agent
	reject: (err?: any) => void
	resolve: () => void
}

export {QueueTable}

class HTTPClient {
	httpagent: http.Agent
	httpsagent: https.Agent
	queue: QueueTable[] = []
	working = false
	memcached = false // true - hold chunks in memory and emit onchunk only once when download is complete
	workingConnections = 0

	constructor(public connections = 16) {
		this.httpsagent = new https.Agent({
			keepAlive: true,
			keepAliveMsecs: 20000,
		})

		this.httpagent = new http.Agent({
			keepAlive: true,
			keepAliveMsecs: 20000,
		})
	}

	get queueSize() {
		return this.queue.length
	}

	workLoop() {
		const value = this.queue.pop()

		if (!value) {
			return
		}

		const params = {
			hostname: value.url.host,
			port: value.url.port,
			path: value.url.pathname + value.url.search,
			agent: value.agent,
			method: 'GET',
			headers: {
				'User-Agent': 'InSync/universal'
			}
		}

		this.workingConnections++
		this.working = this.workingConnections != 0

		const callback = (response: http.IncomingMessage) => {
			if (response.statusCode != 200) {
				this.workingConnections--
				this.working = this.workingConnections != 0

				if ((response.statusCode == 301 || response.statusCode == 302) && response.headers.location) {
					value.onStart = () => {}
					value.url = new urlModule.URL(response.headers.location)
					this.queue.push(value)

					this.work()
				} else {
					this.work()
					value.reject('Status code from server is ' + response.statusCode)
				}

				return
			}

			if (value.onStart) {
				value.onStart(response)
			}

			// UNSAFE - memcached flag can change during work
			let finished = false
			let memcache: Buffer[]

			if (this.memcached) {
				memcache = []
			}

			response.on('data', (chunk: Buffer) => {
				if (this.memcached) {
					memcache.push(chunk)
				} else {
					value.onchunk(chunk)
				}
			})

			response.on('error', (err) => {
				this.workingConnections--
				this.working = this.workingConnections != 0
				this.work()
				value.reject(err)
			})

			response.socket.setMaxListeners(50000)

			response.socket.once('error', (err) => {
				if (finished) {
					return
				}

				this.workingConnections--
				this.working = this.workingConnections != 0
				this.work()
				value.reject(err)
			})

			response.socket.once('close', (isError) => {
				if (finished) {
					return
				}

				if (!isError) {
					return
				}

				this.workingConnections--
				this.working = this.workingConnections != 0
				this.work()
				value.reject('Socket termination error')
			})

			response.on('end', () => {
				this.workingConnections--
				this.working = this.workingConnections != 0
				this.work()

				if (this.memcached) {
					let size = 0

					for (const buff of memcache) {
						size += buff.length
					}

					const newbuff = Buffer.allocUnsafe(size)
					let offset = 0

					for (const buff of memcache) {
						// hi Buffer.write(Buffer) ???

						for (let i = 0; i < buff.length; i++) {
							newbuff[offset + i] = buff[i]
						}

						offset += buff.length
					}

					// mark buffers as dead for gc
					memcache = []

					value.onchunk(newbuff)
				}

				finished = true
				value.resolve()
			})
		}

		if (value.https) {
			https.request(params, callback).end()
		} else {
			http.request(params, callback).end()
		}
	}

	work() {
		while (this.workingConnections < this.connections && this.queue.length != 0) {
			this.workLoop()
		}
	}

	download(url: string, onchunk: (chunk: Buffer) => void, onStart?: (response: http.IncomingMessage) => void): Promise<undefined> {
		return new Promise((resolve, reject) => {
			const urlobj = new urlModule.URL(url)

			if (urlobj.protocol != 'https:' && urlobj.protocol != 'http:') {
				throw new TypeError('Protocol is not supported: ' + urlobj.protocol)
			} else if (urlobj.protocol == 'https:') {
				this.queue.push({
					url: urlobj,
					https: true,
					onchunk: onchunk,
					onStart: onStart,
					reject: reject,
					resolve: resolve,
					agent: this.httpsagent
				})
			} else {
				this.queue.push({
					url: urlobj,
					https: false,
					onchunk: onchunk,
					onStart: onStart,
					reject: reject,
					resolve: resolve,
					agent: this.httpagent
				})
			}

			if (!this.working) {
				this.work()
			}
		})
	}
}

export {HTTPClient}
