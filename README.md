
# InSync

These scripts allows you to create very simple synchronization mechanism over HTTP or HTTPS.

To get started you need to install npm packages needed for compilation:

```
cd project
npm install
```

Then you need to compile it:

```
cd client
../node_modules/.bin/tsc
cd ../server
../node_modules/.bin/tsc
```

And you are done here!

# Usage

## Index structure

Firstly, you need to create index file which contain all required information in following structure:
```json
{
	"syncFiles": ["path/to/file.jar"],
	"syncDirectories": ["onedir", "mods"],

	"downloadFiles": ["userconfig/default_config.cfg"],
	"downloadDirectories": ["config"],

	"syncExcludeFiles": [],
	"syncExcludeDirs": [],

	"noStrictChecks": true
}
```

Keep in mind that bundle.json and all file pathes are relatative to the folder of index.json!

### syncFiles

Contains files for "hard" synchronization (they will be validated on each client startup)

### syncDirectories

Contains directories with files for "hard" synchronization (they will be validated on each client startup).
This option will also tell client to erase all excess files in these directories

### syncExcludeFiles

Files which will act like "downloadFiles" if they are inside "syncDirectories" (they will not be validated or removed)

### syncExcludeDirs

Directories with files which will act like "downloadFiles" if they are inside "syncDirectories" (they will not be validated or removed)

### downloadFiles

Contains files which needs to be downloaded **only** if not present

### downloadDirectories

Contains directories with files which needs to be downloaded **only** if not present. Does not validate directories for any excess files

### noStrictChecks

If `true` - client do not disk intense file valdiation. If `false` - client will validate files by sha256 hashsum

## Server

Usage:

```
node server/output/EntryPoint.js <path to index file.json>
```

This command will start "server" which would generate bundle.json file containing all filesizes and sha256 hash sums for all files specified in index.json

## Client

Usage:

```
node server/output/EntryPoint.js <url to index.json> --output <dir> --connections <num>
```

Output and connections are optional (by default, there are 16 connections and output is being done to current folder)

Doing so will tell client to download index.json and then download bundle.json. Then it will validate all what you spcified in index.json. And after all validations and removals it will download all missing or "broken" files.

# Why?

I made these scripts for easy "minecraft modpacks" distribution to all my friends.

# License

Copyright (c) 2018 DBotThePony

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.