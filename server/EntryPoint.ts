
// Copyright (c) 2018 DBotThePony

// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons
// to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be
// included in all copiesor substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

import {PackageIndex} from '../shared/PackageIndex'
import {PackageFile} from '../shared/PackageFile'
import {PackageBundle} from '../shared/PackageBundle'
import {SimWorkerDispatcher} from '../shared/SimWorkerDispatcher'
import {createRotation, createNewTimer} from '../shared/ConsoleStuff'
import fs = require('fs')

if (process.argv.length == 2) {
	console.log(`You must specify path to bundle index file.`)
	process.exit(0)
}

const bundle = process.argv[2]
const split = bundle.split('/')
split.pop()
const workingDir = split.join('/') + '/'

createNewTimer(createRotation('Reading index file'))

fs.readFile(bundle, (err, data) => {
	if (err) {
		console.error('Error while reading data!')
		console.error(err)
		process.exit(1)
		return
	}

	const obj = JSON.parse(data.toString('utf8'))
	const index = new PackageIndex(obj)
	const packageBundle = new PackageBundle(false, workingDir)

	index.search(workingDir)
	.then((files) => {
		console.log('\nFound ' + files.length + ' files!')

		if (index.noStrictChecks) {
			createNewTimer(createRotation('Calculating file sizes'))
		} else {
			createNewTimer(createRotation('Hashing files'))
		}

		const pfiles: PackageFile[] = []

		new SimWorkerDispatcher<string>(64, files, (sfile) => {
			return new Promise((resolve, reject) => {
				const file = new PackageFile(packageBundle, sfile)
				pfiles.push(file)

				let promise: any

				if (index.noStrictChecks) {
					promise = file.resolveSize()
				} else {
					promise = file.resolveHashsum()
				}

				promise.then(() => {
					resolve()
				})
				.catch((err: any) => {
					console.error('Error while hashing files!')
					console.error(err)
					process.exit(1)
				})
			})
		}, () => {
			createNewTimer(createRotation('Processing'))

			for (const file of pfiles) {
				packageBundle.put(file)
			}

			createNewTimer(createRotation('Writing bundle file'))

			packageBundle.writeFile().then(() => {
				console.log('\nSuccessfully writed bundle to disk!')
				process.exit(0)
			}).catch(err => {
				console.error('Error while writing bundle json file!')
				console.error(err)
				process.exit(1)
			})
		}).work()
	})
})