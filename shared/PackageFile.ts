
// Copyright (c) 2018 DBotThePony

// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons
// to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be
// included in all copiesor substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

import crypto = require('crypto')
import fs = require('fs')
import { PackageBundle } from './PackageBundle'
import { resolve } from 'url';

class PackageFile {
	exists: null | boolean = null
	hashed = false

	hashsum: string | null = null
	writeStream: fs.WriteStream | null = null
	writeHash: crypto.Hash | null = null
	writeStreamCallback: ((chunk: string | Buffer) => void) | null = null
	filesize: number | null = null
	dirs: string[]

	get isClient() {
		return this.parent.isClient
	}

	constructor(public parent: PackageBundle, public path: string, public hashsumExpected?: string, public forceSync = true, public filesizeExpected: number | null = null) {
		if (this.path.substr(0) == '/') {
			this.path = this.path.substr(1)
		}

		const dirs = this.path.split('/')
		const repack = []
		dirs.pop()

		for (const dir of dirs) {
			if (dir == '..') {
				repack.pop()
			} else if (dir != '.') {
				repack.push(dir)
			}
		}

		this.dirs = repack
	}

	outOfSync() {
		return this.isClient && (
			!this.exists ||
			this.forceSync && (this.hashsumExpected != this.hashsum || this.filesizeExpected != this.filesize)
		)
	}

	outOfSyncWeak() {
		if (this.filesizeExpected == null) {
			return this.outOfSync()
		}

		return this.isClient && (
			!this.exists ||
			this.forceSync && this.filesizeExpected != this.filesize
		)
	}

	getFullPath() {
		return this.parent.workingDir + this.path
	}

	isHashed() {
		return this.hashed
	}

	openWriteStream() {
		if (this.writeStreamCallback) {
			return this.writeStreamCallback
		}

		const stream = fs.createWriteStream(this.getFullPath())
		this.hashsum = null
		this.writeHash = crypto.createHash('sha256')
		this.writeStream = stream
		const hash = this.writeHash
		this.filesize = 0
		const self = this

		this.writeStreamCallback = (chunk: string | Buffer) => {
			hash.update(chunk)
			stream.write(chunk)
			self.filesize! += chunk.length
		}

		return this.writeStreamCallback
	}

	closeStream(): Promise<fs.WriteStream> {
		if (!this.writeStream) {
			throw new Error('Stream is not present!')
		}

		return new Promise((resolve, reject) => {
			const stream = this.writeStream!

			stream.on('close', () => {
				resolve(stream)
			})

			stream.close()
			this.hashsum = this.writeHash!.digest('hex')
			this.writeHash = null
			this.exists = true
			this.hashed = true
		})
	}

	createDirs(): Promise<this> {
		if (!this.isClient) {
			throw new Error('Illegal side for createDirs(). Must be client!')
		}

		return new Promise((resolve, reject) => {
			let i = 0
			let buildpath = this.parent.workingDir

			const callback = () => {
				const dir = this.dirs[i]

				if (!dir) {
					resolve(this)
					return
				}

				fs.stat(buildpath + dir + '/', (err, stat) => {
					const logic = () => {
						i++
						buildpath += dir + '/'
						callback()
					}

					const proceed = () => {
						fs.mkdir(buildpath + dir + '/', (err) => {
							if (err && err.code != 'EEXIST') {
								reject(err)
								return
							}

							logic()
						})
					}

					if (err) {
						proceed()
					} else if (stat) {
						if (stat.isDirectory()) {
							logic()
						} else {
							fs.unlink(buildpath + dir + '/', (err) => {
								if (err) {
									reject(err)
								} else {
									proceed()
								}
							})
						}
					} else {
						proceed()
					}
				})
			}

			callback()
		})
	}

	rehash(): Promise<this> {
		return new Promise((resolve, reject) => {
			const path = this.getFullPath()

			try {
				const stream = fs.createReadStream(path)
				const hash = crypto.createHash('sha256')

				stream.pipe(hash)

				stream.on('close', () => {
					const digest = hash.digest('hex')
					this.hashed = true
					this.exists = true
					this.hashsum = digest
					resolve(this)
				})
			} catch(err) {
				if (this.isClient) {
					this.exists = false
					this.hashed = false
					this.hashsum = null
					this.filesize = null
				}

				reject(err)
			}
		})
	}

	updateExists(): Promise<this> {
		return new Promise((resolve, reject) => {
			fs.stat(this.getFullPath(), (err, stats) => {
				if (err) {
					this.exists = false
					resolve(this)
					return
				}

				if (this.forceSync) {
					this.exists = true
				} else {
					this.exists = stats.isFile()
				}

				if (stats.isFile()) {
					this.filesize = stats.size
				}

				resolve(this)
			})
		})
	}

	resolveSize(): Promise<number | null> {
		return new Promise((resolve, reject) => {
			if (this.filesize) {
				resolve(this.filesize)
				return
			}

			if (this.exists == false) {
				resolve(null)
				return
			}

			this.updateExists().then(() => {
				resolve(this.filesize)
			}).catch(reject)
		})
	}

	resolveHashsum(): Promise<this> {
		return new Promise((resolve, reject) => {
			if (!this.hashed) {
				if (this.exists == null) {
					this.updateExists().then(() => {
						this.resolveHashsum().then(resolve).catch(reject)
					}).catch(reject)

					return
				}

				if (this.exists == false) {
					reject('File not exists')
					return
				}

				this.rehash().then(resolve).catch(reject)
				return
			}

			resolve(this)
		})
	}
}

export {PackageFile}
