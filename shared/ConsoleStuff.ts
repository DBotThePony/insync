
// Copyright (c) 2018 DBotThePony

// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons
// to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be
// included in all copiesor substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

import readline = require('readline')

let timer: NodeJS.Timer | null

const removeTimer = () => {
	if (timer) {
		clearInterval(timer)
	}

	timer = null
}

export {removeTimer}

const createNewTimer = (callback: () => string | void) => {
	if (timer) {
		clearInterval(timer)
	}

	process.stdout.write('\n')

	const value = callback()

	if (value) {
		process.stdout.write(value)
	}

	timer = setInterval(() => {
		const value = callback()

		if (!value) {
			return
		}

		readline.clearLine(process.stdout, 0)
		readline.cursorTo(process.stdout, 0)
		process.stdout.write(value)
		readline.cursorTo(process.stdout, 0)
	}, 150)
}

export {createNewTimer}

const createNewTimerC = (callback: () => string | void) => {
	readline.clearLine(process.stdout, 0)
	return createNewTimer(callback)
}

export {createNewTimerC}

const createRotation = (strPrepend: string) => {
	let rotate = 0

	return () => {
		rotate++

		if (rotate == 4) {
			rotate = 0
		}

		if (rotate == 0) {
			return strPrepend + ' |'
		} else if (rotate == 1) {
			return strPrepend + ' /'
		} else if (rotate == 2) {
			return strPrepend + ' --'
		} else if (rotate == 3) {
			return strPrepend + ' \\'
		}
	}
}

export {createRotation}
