
// Copyright (c) 2018 DBotThePony

// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons
// to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be
// included in all copiesor substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

class SimWorkerDispatcher<T> {
	active = 0
	get working() { return this.active != 0 }

	constructor(public maxWorkers = 16, public workQueue: T[], public callback: (arg: T) => Promise<void>, public finish: () => void) {

	}

	push(...values: T[]) {
		this.workQueue.push(...values)
	}

	workLoop() {
		const value = this.workQueue.pop()

		if (!value) {
			return
		}

		this.active++

		this.callback(value).then(() => {
			this.active--

			if (this.active == 0 && this.workQueue.length == 0) {
				this.finish()
			} else if (this.workQueue.length != 0) {
				this.workLoop()
			}
		})
	}

	work() {
		while (this.active < this.maxWorkers && this.workQueue.length != 0) {
			this.workLoop()
		}

		return this
	}
}

export {SimWorkerDispatcher}
