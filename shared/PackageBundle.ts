
// Copyright (c) 2018 DBotThePony

// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons
// to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be
// included in all copiesor substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

import fs = require('fs')
import { PackageFile } from './PackageFile'

interface FileMapEntry {
	path: string
	sha256: string
	size: number
}

class PackageBundle {
	structure: FileMapEntry[] = []

	constructor(public isClient = false, public workingDir = './') {

	}

	put(entry: FileMapEntry | PackageFile) {
		if (this.isClient) {
			throw new Error('Illegal side! Must be server!')
		}

		if (entry instanceof PackageFile) {
			this.structure.push({
				path: entry.path,
				sha256: entry.hashsum!,
				size: entry.filesize!
			})
		} else {
			this.structure.push(entry)
		}
	}

	files() {
		return this.structure
	}

	from(str: string | FileMapEntry[]) {
		if (typeof str == 'string') {
			str = JSON.parse(str)
		}

		this.structure = []

		let i = 0

		for (const entry of <FileMapEntry[]> str) {
			if (!entry.path || (!entry.sha256 && entry.size == null)) {
				throw new Error('Malformed row at ' + i + `(${entry.path}/${entry.sha256}/${entry.size})`)
			}

			i++
			this.structure.push(entry)
		}
	}

	stringify() {
		return JSON.stringify(this.structure)
	}

	write(stream: fs.WriteStream) {
		stream.write(this.stringify())
		return this
	}

	writeFile(): Promise<this> {
		return new Promise((resolve, reject) => {
			fs.writeFile(this.workingDir + './bundle.json', this.stringify(), (err) => {
				if (err) {
					reject(err)
					return
				}

				resolve(this)
			})
		})
	}
}

export {PackageBundle}
