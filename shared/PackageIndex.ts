
// Copyright (c) 2018 DBotThePony

// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons
// to whom the Software is furnished to do so, subject to the
// following conditions:

// The above copyright notice and this permission notice shall be
// included in all copiesor substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
// ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

import fs = require('fs')
import { createNewTimer, createRotation } from "./ConsoleStuff";

class PackageIndex {
	// sync - delete excess files, download missing. On changes - redownload
	syncFiles: string[] = []
	syncDirectories: string[] = []

	// download - download if not present
	downloadFiles: string[] = []
	downloadDirectories: string[] = []

	syncExcludeFiles: string[] = []
	syncExcludeDirs: string[] = []

	// true - don't hashsum files everytime
	// false - always hashsum
	noStrictChecks = true

	isSync(path: string) {
		if (this.syncFiles.includes(path)) {
			return true
		}

		for (const dir of this.syncDirectories) {
			if (path.startsWith(dir)) {
				return true
			}
		}

		return false
	}

	shouldVerify(path: string) {
		if (this.syncExcludeFiles.includes(path)) {
			return false
		}

		for (const dir of this.syncExcludeDirs) {
			if (path.startsWith(dir)) {
				return false
			}
		}

		return this.isSync(path)
	}

	shouldRemove(path: string) {
		if (this.syncExcludeFiles.includes(path)) {
			return false
		}

		for (const dir of this.syncExcludeDirs) {
			if (path.match('^' + dir)) {
				return false
			}
		}

		for (const dir of this.syncDirectories) {
			if (path.match('^' + dir)) {
				return true
			}
		}

		return false
	}

	constructor(public rawinfo: any, public isClient = false) {
		if (!rawinfo) {
			return
		}

		if (rawinfo.syncFiles && typeof rawinfo.syncFiles != 'object') {
			throw new TypeError('syncFiles is not an array!')
		} else if (rawinfo.syncFiles) {
			this.syncFiles = rawinfo.syncFiles
		}

		if (rawinfo.syncDirectories && typeof rawinfo.syncDirectories != 'object') {
			throw new TypeError('syncDirectories is not an array!')
		} else if (rawinfo.syncDirectories) {
			this.syncDirectories = rawinfo.syncDirectories
		}

		if (rawinfo.downloadFiles && typeof rawinfo.downloadFiles != 'object') {
			throw new TypeError('downloadFiles is not an array!')
		} else if (rawinfo.downloadFiles) {
			this.downloadFiles = rawinfo.downloadFiles
		}

		if (rawinfo.downloadDirectories && typeof rawinfo.downloadDirectories != 'object') {
			throw new TypeError('downloadDirectories is not an array!')
		} else if (rawinfo.downloadDirectories) {
			this.downloadDirectories = rawinfo.downloadDirectories
		}

		if (rawinfo.syncExcludeFiles && typeof rawinfo.syncExcludeFiles != 'object') {
			throw new TypeError('syncExcludeFiles is not an array!')
		} else if (rawinfo.syncExcludeFiles) {
			this.syncExcludeFiles = rawinfo.syncExcludeFiles
		}

		if (rawinfo.syncExcludeDirs && typeof rawinfo.syncExcludeDirs != 'object') {
			throw new TypeError('syncExcludeDirs is not an array!')
		} else if (rawinfo.syncExcludeDirs) {
			this.syncExcludeDirs = rawinfo.syncExcludeDirs
		}

		if (rawinfo.noStrictChecks && typeof rawinfo.noStrictChecks != 'boolean') {
			throw new TypeError('noStrictChecks is not a boolean!')
		} else if (rawinfo.noStrictChecks) {
			this.noStrictChecks = rawinfo.noStrictChecks
		}
	}

	search(workingDir: string): Promise<string[]> {
		return new Promise((resolve, reject) => {
			const files: string[] = []

			createNewTimer(createRotation('Scanning directories and building file list'))

			const lookDirectory = (path: string, callOnEnd: () => void) => {
				fs.readdir(workingDir + path, (err, dirfiles) => {
					if (err) {
						if (!this.isClient) {
							console.error('Error while looking through directories!')
							console.error(err)
							process.exit(1)
							return
						}

						callOnEnd()
						return
					}

					if (dirfiles.length == 0) {
						callOnEnd()
						return
					}

					let done = 0

					for (const file of dirfiles) {
						fs.stat(workingDir + path + '/' + file, (err, stat) => {
							if (err) {
								console.error('Error while looking through directories!')
								console.error(err)
								process.exit(1)
								return
							}

							if (stat.isFile()) {
								files.push(path + '/' + file)
								done++

								if (done == dirfiles.length) {
									callOnEnd()
								}
							} else if (stat.isDirectory()) {
								lookDirectory(path + '/' + file, () => {
									done++

									if (done == dirfiles.length) {
										callOnEnd()
									}
								})
							} else {
								done++

								if (done == dirfiles.length) {
									callOnEnd()
								}
							}
						})
					}
				})
			}

			for (const file of this.syncFiles) {
				files.push(file)
			}

			for (const file of this.downloadFiles) {
				files.push(file)
			}

			let done = 0
			const total = this.syncDirectories.length + this.downloadDirectories.length

			for (const dir of this.syncDirectories) {
				lookDirectory(dir, () => {
					done++

					if (done == total) {
						resolve(files)
					}
				})
			}

			for (const dir of this.downloadDirectories) {
				lookDirectory(dir, () => {
					done++

					if (done == total) {
						resolve(files)
					}
				})
			}

			if (done == total) {
				resolve(files)
			}
		})
	}
}

export {PackageIndex}
